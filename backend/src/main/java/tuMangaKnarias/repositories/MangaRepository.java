package tuMangaKnarias.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tuMangaKnarias.domain.Manga;

@Repository
public interface MangaRepository extends JpaRepository<Manga,Integer> {

}
