package tuMangaKnarias.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tuMangaKnarias.domain.Chapter;

import java.util.List;

@Repository
public interface ChapterRepository extends JpaRepository<Chapter,Integer> {

    List<Chapter> findByManga(int manga);
}
