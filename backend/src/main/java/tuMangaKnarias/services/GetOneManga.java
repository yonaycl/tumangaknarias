package tuMangaKnarias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tuMangaKnarias.domain.Manga;
import tuMangaKnarias.repositories.MangaRepository;

@Service
public class GetOneManga {

    private MangaRepository mangaRepository;

    @Autowired
    public GetOneManga(MangaRepository mangaRepository) {
        this.mangaRepository = mangaRepository;
    }

    public Manga execute(int idOfManga) {
        return mangaRepository.findOne(idOfManga);
    }
}
