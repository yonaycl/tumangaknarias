package tuMangaKnarias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tuMangaKnarias.domain.Manga;
import tuMangaKnarias.repositories.MangaRepository;

@Service
public class CreateManga {

    private MangaRepository mangaRepository;

    @Autowired
    public CreateManga(MangaRepository mangaRepository){

        this.mangaRepository = mangaRepository;
    }

    public void execute(Manga manga) {
        mangaRepository.save(manga);
    }
}
