package tuMangaKnarias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tuMangaKnarias.domain.Chapter;
import tuMangaKnarias.repositories.ChapterRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RemoveOneImageOfChapter {
    private ChapterRepository chapterRepository;

    @Autowired
    public RemoveOneImageOfChapter(ChapterRepository chapterRepository){

        this.chapterRepository = chapterRepository;
    }

    public void execute(Chapter chapter) {
        List<Chapter> mangas = chapterRepository.findByManga(chapter.getManga());
        List <Chapter> chapters = mangas.stream().filter(x-> x.getChapter() == chapter.getChapter()).collect(Collectors.toList());
        Chapter theChapter = chapters.stream().filter(x-> x.getImage().equals(chapter.getImage())).collect(Collectors.toList()).get(0);
        chapterRepository.delete(theChapter.getId());
    }
}
