package tuMangaKnarias.services;

import org.springframework.web.multipart.MultipartFile;
import tuMangaKnarias.domain.Chapter;


public interface S3Services {
    void downloadFile(String keyName, int manga, int chapter);
    String uploadFile(MultipartFile uploadFilePath, String nameOfManga, String chapter);
    void removeFile(Chapter keyName);
}
