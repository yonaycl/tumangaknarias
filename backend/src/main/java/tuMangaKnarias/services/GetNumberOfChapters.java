package tuMangaKnarias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tuMangaKnarias.domain.Chapter;
import tuMangaKnarias.repositories.ChapterRepository;

import java.util.Comparator;
import java.util.List;

@Service
public class GetNumberOfChapters {

    private ChapterRepository chapterRepository;

    @Autowired
    public GetNumberOfChapters(ChapterRepository chapterRepository){

        this.chapterRepository = chapterRepository;
    }
    public int execute(int idOfManga) {
        System.out.println(chapterRepository.findAll().size());
        List<Chapter> mangas = chapterRepository.findByManga(idOfManga);
        if(mangas.size()!=0) {
            Chapter chapter = mangas.stream().max(Comparator.comparing(Chapter::getChapter)).get();
            return chapter.getChapter();
        }
        return 0;
    }
}
