package tuMangaKnarias.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import org.springframework.web.multipart.MultipartFile;
import tuMangaKnarias.domain.Chapter;

@Service
public class S3ServicesImpl implements S3Services {

    private Logger logger = LoggerFactory.getLogger(S3ServicesImpl.class);

    @Autowired
    private AmazonS3 s3client;

    @Value("${jsa.s3.bucket}")
    private String bucketName;

    @Override
    public void downloadFile(String keyName, int manga, int chapter) {
        try {
            System.out.println("Downloading an object");
            File f = new File("C://Users//yonay//Pictures//TFG//" + manga + "//" + chapter);
            if (!f.exists()) {
                new File("C://Users//yonay//Pictures//TFG//" + manga + "//" + chapter).mkdirs();
            }
            S3Object s3object = s3client.getObject(new GetObjectRequest(bucketName + "/" + manga + "/" + chapter + "/", keyName));
            S3ObjectInputStream objectContent = s3object.getObjectContent();
            IOUtils.copy(objectContent, new FileOutputStream("C://Users//yonay//Pictures//TFG//" + manga + "//" + chapter + "//" + keyName));
            logger.info("===================== Import File - Done! =====================");

        } catch (AmazonServiceException ase) {
            logger.info("Caught an AmazonServiceException from GET requests, rejected reasons:");
            logger.info("Error Message:    " + ase.getMessage());
            logger.info("HTTP Status Code: " + ase.getStatusCode());
            logger.info("AWS Error Code:   " + ase.getErrorCode());
            logger.info("Error Type:       " + ase.getErrorType());
            logger.info("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException: ");
            logger.info("Error Message: " + ace.getMessage());
        } catch (IOException ioe) {
            logger.info("IOE Error Message: " + ioe.getMessage());
        }
    }


    @Override
    public String uploadFile(MultipartFile multipartFile, String nameOfManga, String chapter) {

        String fileUrl = "";
        boolean existFile = false;

        try {
            try {
                S3Object folder = s3client.getObject(bucketName, nameOfManga + "/" + chapter + "/");
                existFile = true;
            } catch (Exception e) {
            }
            if (!existFile) {
                s3client.putObject(bucketName, nameOfManga + "/" + chapter + "/", "");
            }
            File file = convertMultiPartToFile(multipartFile);
            String fileName = generateFileName(multipartFile);
            fileUrl = bucketName + "/" + nameOfManga + "/" + chapter + fileName;
            uploadFileTos3bucket(fileName, file, chapter, nameOfManga);
            file.delete();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileUrl;
    }

    @Override
    public void removeFile(Chapter chapter) {
        s3client.deleteObject(new DeleteObjectRequest(bucketName + "/" + chapter.getManga() + "/" + chapter.getChapter() + "/", chapter.getImage()));
    }

    private String generateFileName(MultipartFile multiPart) {
        return multiPart.getOriginalFilename();
    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public void uploadFileTos3bucket(String keyName, File uploadFilePath, String chapter, String nameOfManga) {
        try {
            s3client.putObject(new PutObjectRequest(bucketName + "/" + nameOfManga + "/" + chapter + "/", keyName, uploadFilePath));
            logger.info("===================== Upload File - Done! =====================");

        } catch (AmazonServiceException ase) {
            logger.info("Caught an AmazonServiceException from PUT requests, rejected reasons:");
            logger.info("Error Message:    " + ase.getMessage());
            logger.info("HTTP Status Code: " + ase.getStatusCode());
            logger.info("AWS Error Code:   " + ase.getErrorCode());
            logger.info("Error Type:       " + ase.getErrorType());
            logger.info("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException: ");
            logger.info("Error Message: " + ace.getMessage());
        }
    }
}
