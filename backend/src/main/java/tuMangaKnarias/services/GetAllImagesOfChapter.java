package tuMangaKnarias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tuMangaKnarias.domain.Chapter;
import tuMangaKnarias.repositories.ChapterRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GetAllImagesOfChapter {
    private ChapterRepository chapterRepository;

    @Autowired
    public GetAllImagesOfChapter(ChapterRepository chapterRepository){

        this.chapterRepository = chapterRepository;
    }

    public List<Chapter> execute(int idOfManga, int idOfChapter) {
        List<Chapter> mangas = chapterRepository.findByManga(idOfManga);
        return mangas.stream()
                .filter(manga-> manga.getChapter() == idOfChapter)
                .collect(Collectors.toList());
    }
}
