package tuMangaKnarias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tuMangaKnarias.domain.Chapter;
import tuMangaKnarias.repositories.ChapterRepository;

@Service
public class SaveReferenceOfFile {

    private ChapterRepository chapterRepository;

    @Autowired
    public SaveReferenceOfFile(ChapterRepository chapterRepository){

        this.chapterRepository = chapterRepository;
    }

    public void execute(Chapter chapter) {
        chapterRepository.save(chapter);
    }
}
