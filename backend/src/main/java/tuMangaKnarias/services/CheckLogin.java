package tuMangaKnarias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tuMangaKnarias.domain.LoginDTO;
import tuMangaKnarias.domain.User;
import tuMangaKnarias.repositories.UserRepository;

@Service
public class CheckLogin {

    private UserRepository userRepository;

    @Autowired
    public CheckLogin(UserRepository userRepository){

        this.userRepository = userRepository;
    }

    public User execute(LoginDTO loginDTO) {
        return userRepository.findByEmail(loginDTO.getEmail());
    }
}
