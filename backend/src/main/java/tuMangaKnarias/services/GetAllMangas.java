package tuMangaKnarias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tuMangaKnarias.domain.Manga;
import tuMangaKnarias.repositories.MangaRepository;

import java.util.List;

@Service
public class GetAllMangas {

    private MangaRepository mangaRepository;

    @Autowired
    public GetAllMangas(MangaRepository mangaRepository){

        this.mangaRepository = mangaRepository;
    }

    public List<Manga> execute() {
        return mangaRepository.findAll();
    }
}
