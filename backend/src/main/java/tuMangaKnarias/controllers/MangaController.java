package tuMangaKnarias.controllers;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tuMangaKnarias.domain.Chapter;
import tuMangaKnarias.domain.Manga;
import tuMangaKnarias.services.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.CompletableFuture.runAsync;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
public class MangaController {

    private GetAllMangas getAllMangas;
    private CreateManga createManga;
    private GetOneManga getOneManga;
    private S3Services s3Services;
    private SaveReferenceOfFile saveReferenceOfFile;
    private RemoveOneImageOfChapter removeOneImageOfChapter;
    private GetNumberOfChapters getNumberOfChapters;
    private GetAllImagesOfChapter getAllImagesOfChapter;

    @Autowired
    public MangaController(GetAllMangas getAllMangas,
                           CreateManga createManga,
                           GetOneManga getOneManga,
                           S3Services s3Services,
                           SaveReferenceOfFile saveReferenceOfFile,
                           RemoveOneImageOfChapter removeOneImageOfChapter,
                           GetNumberOfChapters getNumberOfChapters,
                           GetAllImagesOfChapter getAllImagesOfChapter) {

        this.getAllMangas = getAllMangas;
        this.createManga = createManga;
        this.getOneManga = getOneManga;
        this.s3Services = s3Services;
        this.saveReferenceOfFile = saveReferenceOfFile;
        this.removeOneImageOfChapter = removeOneImageOfChapter;
        this.getNumberOfChapters = getNumberOfChapters;
        this.getAllImagesOfChapter = getAllImagesOfChapter;
    }

    @GetMapping("/")
    public List<Manga> index() {
        List<Manga> mangas = getAllMangas.execute();
        Collections.sort(mangas);
        return mangas;
    }

    @GetMapping("/chapters/{idOfManga}")
    public int chapters(@PathVariable("idOfManga") int idOfManga) {
        return getNumberOfChapters.execute(idOfManga);
    }


    @ResponseBody
    @PostMapping(value = "/createManga")
    public void createManga(@RequestBody Manga manga) {
        createManga.execute(manga);
    }

    @GetMapping("/manga/{id}")
    public Manga showManga(@PathVariable("id") int idOfManga) {
        return getOneManga.execute(idOfManga);
    }

    @GetMapping("/manga/{idOfManga}/{idOfChapter}")
    public CompletableFuture<List> showChapter(@PathVariable("idOfManga") int idOfManga, @PathVariable("idOfChapter") int idOfChapter) {
        List<Chapter> chapterOfManga = getAllImagesOfChapter.execute(idOfManga, idOfChapter);
        List<String> images = new ArrayList<>();
        Collections.sort(chapterOfManga);

        return CompletableFuture.supplyAsync(()-> {
            chapterOfManga.forEach(x -> {
                File f = new File("C://Users//yonay//Pictures//TFG//" + idOfManga + "//"+idOfChapter+"//" + x.getImage());
                if (f.exists() && !f.isDirectory()) {
                    String imageBase64 = readAndEncodeFile(f);
                    images.add(imageBase64);
                } else {
                    s3Services.downloadFile(x.getImage(), x.getManga(), x.getChapter());
                }
            });
            return "";
        }).thenApply((response)-> {
            chapterOfManga.forEach(x -> {
                File f = new File("C://Users//yonay//Pictures//TFG//" + idOfManga + "//" + idOfChapter + "//" + x.getImage());
                images.add(readAndEncodeFile(f));
            });
            return images;
        });
    }

    private String readAndEncodeFile(File file) {
        try{
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            return new String(Base64.encodeBase64(bytes), "UTF-8");
        }catch (Exception ignored){
            return "";
        }
    }

    @PostMapping("/saveImageOfChapter")
    public void saveReferenceOfFile(@RequestBody Chapter chapter) {
        saveReferenceOfFile.execute(chapter);
    }

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestPart(value = "file") MultipartFile file,
                             @RequestParam("chapter") String chapter, @RequestParam("manga") String manga) {
        return s3Services.uploadFile(file,manga, chapter);
    }

    @DeleteMapping("/deleteFile")
    public void deleteFile(@RequestBody Chapter chapter) {
        s3Services.removeFile(chapter);
        removeOneImageOfChapter.execute(chapter);
    }
}
