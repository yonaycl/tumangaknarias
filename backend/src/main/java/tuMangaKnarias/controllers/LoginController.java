package tuMangaKnarias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tuMangaKnarias.domain.LoginDTO;
import tuMangaKnarias.domain.User;
import tuMangaKnarias.services.CheckLogin;

@RestController
public class LoginController {
    private CheckLogin checkLogin;

    @Autowired
    public LoginController(CheckLogin checkLogin){

        this.checkLogin = checkLogin;
    }

    @CrossOrigin(origins = "http://localhost:8080")
    @ResponseBody
    @PostMapping("/login")
    public User login(@RequestBody LoginDTO loginDTO){
        return checkLogin.execute(loginDTO);
    }
}
