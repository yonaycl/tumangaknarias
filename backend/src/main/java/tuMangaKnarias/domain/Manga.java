package tuMangaKnarias.domain;

import javax.persistence.*;

@Entity
@Table(name="mangas")
public class Manga implements Comparable<Manga>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String image;
    private String description;
    private int chapters;
    private int score;
    private String genders;


    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public int getChapters() {
        return chapters;
    }

    public int getScore() {
        return score;
    }

    public String getGenders() {
        return genders;
    }

    @Override
    public String toString() {
        return "Manga{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                ", chapters=" + chapters +
                ", score=" + score +
                ", genders=" + genders +
                '}';
    }

    @Override
    public int compareTo(Manga manga) {
        if(getScore() < manga.getScore()){
            return 1;
        }else if(getScore() > manga.getScore()){
            return -1;
        }
        return 0;
    }
}
