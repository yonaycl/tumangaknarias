package tuMangaKnarias.domain;

import javax.persistence.*;

@Entity
@Table(name="chapters")
public class Chapter implements Comparable<Chapter>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String image;
    private int manga;
    private int chapter;

    public Chapter(){

    }

    public Chapter(String image, int manga, int chapter) {
        this.image = image;
        this.manga = manga;
        this.chapter = chapter;
    }



    public String getImage() {
        return image;
    }

    public int getManga() {
        return manga;
    }

    public int getChapter() {
        return chapter;
    }

    @Override
    public String toString() {
        return "Chapter{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", manga=" + manga +
                ", chapter=" + chapter +
                '}';
    }

    public int getId() {
        return id;
    }



    @Override
    public int compareTo(Chapter chapter) {
        if (getImage() == null || chapter.getImage() == null) {
            return 0;
        }
        String image = getImage();
        String chapterImage = chapter.getImage();
        int pointImage = image.indexOf('.');
        int pointChapterImage = chapterImage.indexOf('.');
        int numberOfImage = Integer.parseInt(image.substring(0,pointImage));
        int numberOfChapterImage = Integer.parseInt(chapterImage.substring(0,pointChapterImage));
        if(numberOfImage < numberOfChapterImage){
            return -1;
        }else if(numberOfImage > numberOfChapterImage){
            return 1;
        }
        return 0;
    }
}
