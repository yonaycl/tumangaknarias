import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Login from '@/components/Login'
import Mangas from '@/components/Mangas'
import MangaDetail from '@/components/MangaDetail'
import CreateManga from '@/components/CreateManga'
import InsertChapter from '@/components/InsertChapter'
import ChapterOfManga from '@/components/ChapterOfManga' 

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/mangas',
      name: 'Mangas',
      component: Mangas
    },
    {
      path: '/createManga',
      name: 'createManga',
      component: CreateManga
    },
    {
      path: '/manga/:id',
      name: 'MangaDetail',
      component: MangaDetail
    },
    {
      path: '/manga/:id/insertChapter',
      name: 'InsertChapter',
      component: InsertChapter
    },
    {
      path: '/manga/:id/chapter/:id',
      name: 'ChapterOfManga',
      component: ChapterOfManga
    }
  ]
})
