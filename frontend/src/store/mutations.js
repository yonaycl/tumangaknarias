import {USER_LOGIN, MANGA_USING} from './actions_types'
export default {
    [USER_LOGIN] : (state, userName) => {
        state.userLogin = userName
    },

    [MANGA_USING] : (state, number) => {
        state.mangaUsing = number
    }
}    
