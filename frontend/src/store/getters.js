import {USER_LOGIN, MANGA_USING} from './actions_types'

export default {
    [USER_LOGIN]: (state) => state.userLogin,
    [MANGA_USING]: (state) => state.mangaUsing
}

