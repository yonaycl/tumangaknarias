import {USER_LOGIN, MANGA_USING} from './actions_types'

export default {
    [USER_LOGIN]: ({ commit }, userName) => {
        commit(USER_LOGIN, userName)
    },

    [MANGA_USING]: ({ commit }, number) => {
        commit(MANGA_USING, number)
    }    
}    
