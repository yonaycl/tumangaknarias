/* eslint-disable */
const english ={
    "Hola": "Hello",
    "Biblioteca":"Library",
    "Inicio":"Login",
    "Crear manga": "Create manga",
    "Descripción": "Description",
    "Añadir Capítulo": "Add chapter",
    "CERRAR SESIÓN": "LOG OUT",
    "Título": "Title",
    "Puntuación": "Scores",
    "Seleccionar archivo": "Select File" 
  }
  
  export default english