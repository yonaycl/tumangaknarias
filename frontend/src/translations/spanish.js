/* eslint-disable */
const spanish ={
    "Hello": "Hola",
    "Library": "Biblioteca",
    "Login": "Inicio",
    "Create manga": "Crear manga",
    "Description": "Descripción",
    "Add chapter": "Añadir Capítulo",
    "LOG OUT": "CERRAR SESIÓN", 
    "Title": "Título",
    "Scores": "Puntuación", 
    "Select File": "Seleccionar archivo"
  }
  
  export default spanish