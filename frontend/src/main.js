// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import english from './translations/english'
import spanish from './translations/spanish'
import vuexI18n from 'vuex-i18n'
import getters from './store/getters'
import actions from './store/actions'
import mutations from './store/mutations'

import {
    Vuetify,
    VApp,
    VNavigationDrawer,
    VFooter,
    VList,
    VSelect,
    VBtn,
    VIcon,
    VForm,
    VCard,
    VTextField,
    VExpansionPanel,
    VGrid,
    VMenu,
    VToolbar,
    VCheckbox,
    transitions
} from 'vuetify'

import '../node_modules/vuetify/src/stylus/app.styl'
import 'vue2-dropzone/dist/vue2Dropzone.css'

Vue.use(Vuetify, {
    components: {
        VApp,
        VNavigationDrawer,
        VFooter,
        VList,
        VSelect,
        VBtn,
        VIcon,
        VForm,
        VTextField,
        VGrid,
        VMenu,
        VCard,
        VCheckbox,
        VToolbar,
        VExpansionPanel,
        transitions
    },
    theme: {
        primary: '#42A5F5',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
    }
})

Vue.use(Vuex)
Vue.config.productionTip = false
var userLang = navigator.language || navigator.userLanguage
const store = new Vuex.Store({
    state: {
        userLogin: '',
        mangaUsing: '',
    },
    actions,
    mutations,
    getters
})
Vue.use(vuexI18n.plugin, store)
Vue.i18n.add('en', english)
Vue.i18n.add('es', spanish)
Vue.i18n.set(userLang)



/* eslint-disable no-new */
new Vue({
    store,
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})